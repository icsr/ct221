function [perf1, perf2, perf] = checkModel(outcomeFolder, model)

load(sprintf('%s/performance.mat', outcomeFolder));

load(sprintf('%s/try_%d.mat', outcomeFolder, bestOutcomeValue));

h = figure;
set(h,'DefaultTextFontSize',20)
nbins = 2000;

histogram([err(1,:), err(2,:)],nbins);
grid on
title(sprintf('Histograma do Erro de Predição Global\npara o Rio Camargos pelo modelo %d', model));

set(h, 'Units', 'Normalized', 'Position', [0,0,1,1]);

savePlot(h, sprintf('%s_hist_%d.jpg', outcomeFolder, bestOutcomeValue));
savefig(h, sprintf('%s_hist_%d.fig', outcomeFolder, bestOutcomeValue));

close all

end