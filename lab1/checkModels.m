performances = [];

x = 1 : 6;

% Modelo 1
[perf1, perf2, perf] = checkModel('resultados/lm20', 1);
performances = [performances; [perf1, perf2, perf]];

% Modelo 2
[perf1, perf2, perf] = checkModel('resultados/lm20-30-25', 2);
performances = [performances; [perf1, perf2, perf]];

% Modelo 3
[perf1, perf2, perf] = checkModel('resultados/rp190', 3);
performances = [performances; [perf1, perf2, perf]];

% Modelo 4
[perf1, perf2, perf] = checkModel('resultados/cgp110', 4);
performances = [performances; [perf1, perf2, perf]];

% Modelo 5
[perf1, perf2, perf] = checkModel('resultados/br155', 5);
performances = [performances; [perf1, perf2, perf]];

%Modelo 6
[perf1, perf2, perf] = checkModel('resultados/cgp48,110,110,110,24', 6);
performances = [performances; [perf1, perf2, perf]];

h = figure;

plot(x, performances(:,1), 'bo-', x, performances(:,2), 'ro-', x, performances(:,3), 'ko-');
grid on
xlabel('Modelo');
ylabel('MSE');
legend('MSE Rio 1', 'MSE Rio 2', 'MSE Global', 'location', 'northeast');
set(h, 'Units', 'normalized', 'Position', [0, 0, 1, 1]); 
title('Comparativo de Performance dos Modelos')
xticks(x);

savePlot(h, 'resultados/comparativo.jpg');

h = figure;

performances(5,:) = [];
x(5) = [];

plot(x, performances(:,1), 'bo-', x, performances(:,2), 'ro-', x, performances(:,3), 'ko-');
grid on
xlabel('Modelo');
ylabel('MSE');
legend('MSE Rio 1', 'MSE Rio 2', 'MSE Global', 'location', 'northeast');
set(h, 'Units', 'normalized', 'Position', [0, 0, 1, 1]); 
title('Comparativo de Performance dos 5 melhores Modelos')
xticks(x);

savePlot(h, 'resultados/comparativoTop5.jpg');

close all
