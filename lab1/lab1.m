clear
close all
clc

tries = 10;

% Modelo 1
runModel(20, 'trainlm', tries, 'resultados/lm20');

% Modelo 2
runModel([20,30,25], 'trainlm', tries, 'resultados/lm20-30-25');

% Modelo 3
runModel(190, 'trainrp', tries, 'resultados/rp190');

% Modelo 4
runModel(110, 'traincgp', tries, 'resultados/cgp110');

% Modelo 5
runModel(155, 'trainbr', tries, 'resultados/br155');

%Modelo 6
runModel([48,110,110,110,24], 'traincgp', tries, 'resultados/cgp48,110,110,110,24');



