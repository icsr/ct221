function plotOutcome(outcomeFolder, tries)

camargos = load('dados/camargos.txt');
furnas = load('dados/furnas.txt');

pauseTime = 1;

[lin1, cols1] = size(camargos);
[lin2, cols2] = size(furnas);

perfs = nan(1,max(size(tries)));
perfsIndex = 0;
bestOutcomeValue = 0;
bestOutcomeIndex = 0;

for t = tries
        
    close all    
    load(sprintf('%s/try_%d.mat', outcomeFolder, t));
    
    Y1 = [camargos(1, :), camargos(2, :)];
    YS1 = nan(1,2*cols2);
    
    Y2 = [furnas(1, :), furnas(2, :)];
    YS2 = nan(1,2*cols2);
    
    for i = 1 : lin1 - 2        
        P1 = [camargos(i, :)'; camargos(i+1, :)'];
        T1 = camargos(i+2, :)';
        Y1 = [Y1, T1'];
        
        P2 = [furnas(i, :)'; furnas(i+1, :)'];
        T2 = furnas(i+2, :)';
        Y2 = [Y2, T2'];
        
        P = [P1; P2];
        T = [T1; T2];
        
        TS = sim(net, P);
        YS1 = [YS1,  TS(1 : cols1)'];
        YS2 = [YS2, TS(cols1 + 1 : 2*cols1)'];
    end
      
    k = find(~isnan(YS1));
    perf1 = mse(net,Y1(k),YS1(k));
    perf2 = mse(net,Y2(k),YS2(k));  
    Y = [Y1(k); Y2(k)];
    YS = [YS1(k); YS2(k)];
    err =  Y - YS;
    perf = mse(err);
        
    save(sprintf('%s/try_%d.mat', outcomeFolder, t), 'net', 'perf', 'perf1', 'perf2', 'err', 'Y', 'YS');
    
    perfsIndex = perfsIndex + 1;
    perfs(perfsIndex) = perf;
    if(min(perfs) == perf)
        bestOutcomeIndex = perfsIndex;
        bestOutcomeValue = t;
    end
    
end

    close all    

    fprintf('\n%s\nMelhor rede: try_%d\nMSE: %0.2e\n', outcomeFolder, bestOutcomeValue, perfs(bestOutcomeIndex));

    save(sprintf('%s/performance.mat', outcomeFolder), 'perfs', 'bestOutcomeValue');

    load(sprintf('%s/try_%d.mat', outcomeFolder, bestOutcomeValue));

    t = bestOutcomeValue;

    X = 1 : max(size(Y1));
    
    lineWidth = 8;
     markerSize = 20;
    
    h = figure;
    grid on
    hold on
    h1 = plot(X', Y1', 'b-', 'LineWidth', lineWidth, 'MarkerSize', markerSize);
    h2 = plot(X((lin1 - 1)*cols2 + 1:end)', Y1((lin1 - 1)*cols2 + 1:end)', 'k-', 'LineWidth', lineWidth, 'MarkerSize', markerSize);
    h3 = plot(X', YS1', 'r:', 'LineWidth', lineWidth, 'MarkerSize', markerSize);
    title(sprintf('Vazão do Rio Camargos\n(MSE: %0.2e)', perf1));
    xlabel('Mês');
    ylabel('Vazão');
    legend('Real Registrado', 'Trecho Predito', 'Saída Predita', 'location', 'north');
    savePlot(h, sprintf('%s_r1.jpg', outcomeFolder));

    xlim([(lin1 - 2)*cols1, lin1*cols2]);
    set(h1,'marker','<')
    set(h2,'marker','>')
    set(h3,'marker','s')
    
    legend('Real Registrado', 'Trecho Predito', 'Saída Predita', 'location', 'northeast');
    pause(pauseTime);
    savePlot(h, sprintf('%s_r1_zoomed.jpg', outcomeFolder));

    h = figure;
    grid on
    hold on
    h1 = plot(X', Y2', 'b-', 'LineWidth', lineWidth, 'MarkerSize', markerSize);
    h2 = plot(X((lin1 - 1)*cols2 + 1:end)', Y2((lin1 - 1)*cols2 + 1:end)', 'k-', 'LineWidth', lineWidth, 'MarkerSize', markerSize);
    h3 = plot(X', YS2', 'r--', 'LineWidth', lineWidth, 'MarkerSize', markerSize);
    title(sprintf('Vazão do Rio Furnas\n(MSE: %0.2e)', perf2));
    xlabel('Mês');
    ylabel('Vazão');
    legend('Real Registrado', 'Trecho Predito', 'Saída Predita', 'location', 'north');       
    savePlot(h, sprintf('%s_r2.jpg', outcomeFolder));
    
    xlim([(lin1 - 2)*cols1, lin1*cols2]);   
    set(h1,'marker','<')
    set(h2,'marker','>')
    set(h3,'marker','s')
    
    legend('Real Registrado', 'Trecho Predito', 'Saída Predita', 'location', 'northeast');
    pause(pauseTime);
    savePlot(h, sprintf('%s_r2_zoomed.jpg', outcomeFolder));
       
    close all

end