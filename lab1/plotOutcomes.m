clear
close all
clc

tries = 1:10;

% Modelo 1
plotOutcome('resultados/lm20', tries);

% Modelo 2
plotOutcome('resultados/lm20-30-25', tries);

% Modelo 3
plotOutcome('resultados/rp190', tries);

% Modelo 4
plotOutcome('resultados/cgp110', tries);

% Modelo 5
plotOutcome('resultados/br155', tries);

%Modelo 6
plotOutcome('resultados/cgp48,110,110,110,24', tries);

