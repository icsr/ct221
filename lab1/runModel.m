function runModel(N, trainFcn, tries, folder)

camargos = load('dados/camargos.txt');
furnas = load('dados/furnas.txt');

if ~exist('resultados', 'dir')
    mkdir resultados
end

if ~exist(folder, 'dir')
    mkdir(folder);
end

[lin1, cols1] = size(camargos);
[lin2, cols2] = size(furnas);

assert(lin1 == lin2);
assert(cols1 == cols2);


for t = 1 : tries
    clear net
    
    P1 = [];
    T1 = [];
    P2 = [];
    T2 = [];
    
    for i = 1 : lin1 - 3
        P1 = [P1 [camargos(i, :)'; camargos(i+1, :)']];
        T1 = [T1 camargos(i+2, :)'];
    end
    
    for i = 1 : lin2 - 3
        P2 = [P2 [furnas(i, :)'; furnas(i+1, :)']];
        T2 = [T2 furnas(i+2, :)'];
    end
    
    P = [P1; P2];
    T = [T1; T2];
    
    net = feedforwardnet(N, trainFcn);
    net = configure(net,P,T);
    
    net.divideFcn = 'dividerand';
    net.divideParam.trainRatio = 1;
    net.divideParam.valRatio = 0;
    net.divideParam.testRatio = 0;
    
    net = init(net);
    
    net.layers{1}.transferFcn = 'tansig';
    net.layers{2}.transferFcn = 'purelin';
    net.performFcn = 'mse';
    net.trainParam.epochs = 10^6;
    net.trainParam.time = 240;
    net.trainParam.lr = 0.2;
    net.trainParam.min_grad = 10^-18;
    net.trainParam.max_fail = 10^3;
    [net, ~] = train(net,P,T);
        
    save(sprintf('%s/try_%d.mat', folder, t), 'net');
    
end

plotOutcome(folder, tries);

end