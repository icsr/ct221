function savePlot(h, file)

set(h, 'Units', 'Normalized', 'Position', [0,0,1,1]);

set(gcf,'PaperPositionMode', 'auto');

set(gcf, 'ResizeFcn', 'resize_fcn');

resize_fcn

set(gca,'FontSize',30)

hline = findobj(gca, 'type', 'line');
set(hline,'LineWidth', 3)

saveas(h, file);

end