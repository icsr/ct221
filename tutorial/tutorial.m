clear all
clc
x=0:0.1:10;
y=0:0.1:10;

[X Y] = meshgrid(x,y);

Z = X.*sin(4*X)+1.1*Y.*sin(2*Y);

mesh(X,Y,Z);
title('Function to be interpolated by Neural Network');
xlabel('');
ylabel('');
zlabel('');
grid on
hold on


dim=10^4;
Xrna=10*rand(1,dim);
Yrna=10*rand(1,dim);
Zrna = Xrna.*sin(4*Xrna)+1.1*Yrna.*sin(2*Yrna);
plot3(Xrna, Yrna, Zrna, '.b');
title('Training with 10000 points');

P=[Xrna; Yrna];
T=Zrna;

net = feedforwardnet(30);
net = configure(net, P, T);

net.inputs{1}.processParams{2}.ymin=0;
net.inputs{1}.processParams{2}.ymax=1;

net.outputs{2}.processParams{2}.ymin=0;
net.outputs{2}.processParams{2}.ymax=1;

net.divideParam.trainRation=0.7;
net.divideParam.valRatio=0.15;
net.divideParam.testRatio=0.15;

net=init(net);
net.trainParam.showWindow=true;
net.layers{1}.dimensions=30;
net.layers{1}.transferFcn='tansig';
net.layers{2}.transferFcn='purelin';
net.performFcn='mse';
net.trainFcn='trainlm';
net.trainParam.epochs=10^4;
net.trainParam.time=120;
net.trainParam.lr=0.2;
net.trainParam.min_grad=10^-8;
net.trainParam.max_fail=1000;

[net tr] = train(net, P, T);

%testing
aux=size(X);

Ztest=zeros(aux(1), aux(2));

for i=1:aux(1)
    for j=1:aux(2)
        Paux=[X(i,j) Y(i,j)]';
        Ztest(i,j)=net(Paux);
    end
end
figure
hold off
plot3(Xrna, Yrna, Zrna, '.r');
hold on
mesh(X,Y,Ztest);
grid

%performance graphs
trIn(1,:)=P(1,tr.trainInd);
trIn(2,:)=P(2,tr.trainInd);
trOut=T(tr.trainEnd);

vIn(1,:)=P(1,tr.valInd);
vIn(2,:)=P(2,tr.valInd);
vOut(1,:)=T(tr.valInd);

tsIn(1,:)=P(1,tr.testInd);
tsIn(2,:)=P(2,tr.testInd);
tsOut=T(tr.testInd);

figure(2)
plot3(trIn(1,:), vIn(2,:), trOut, '.b');
hold on
plot3(vIn(1,:), vIn(2,:), vOut, '.r');
plot3(tsIn(1,:), tsIn(2,:), tsOut, '.y');
grid on
legend('Blue: training', 'Red: validation', 'Yellow: test');

figure(3)
plotperf(tr)

Ztarget=T;
Zout=net(P);
trOut=Zout(tr.trainInd);
vOut=Zout(tr.valInd);
tsOut=Zout(tr.testInd);
trTarg=Ztarget(tr.trainInd);
vTarg=Ztarget(tr.valInd);
tsTarg=Ztarget(tr.testInd);

figure(4)
plotregression(trTarg, trOut, 'Training', vtarg, vOut, 'Validation', tsTarg, tsOut, 'Test');


%lab mostra a vazão por ano do rio poe tudo pra treinamento até a penúltima
%linha e daí prevê a última (são dois rios)